#!/bin/bash
#
args=("$@")
PHP_V=${args[0]}
MARIADB_V=${args[1]}
DB_NAME=${args[2]}
DB_USERNAME=${args[3]}
DB_PASSWORD=${args[4]}
DB_CHARACTER_SET=${args[5]}
DB_COLLATE=${args[6]}
HOSTNAME=${args[7]}
SSL_C=${args[8]}
SSL_ST=${args[9]}
SSL_L=${args[10]}
SSL_O=${args[11]}
SSL_OU=${args[12]}
EMAIL=${args[13]}
HTTP_SERVER=${args[14]}

# Install some handy things
apt-get update
apt-get install -qy software-properties-common apt-transport-https lsb-release ca-certificates puppet python-apt linux-headers-amd64 build-essential dkms libicu-dev openssl libssl-ocaml curl git-core unzip tidy debconf-utils figlet toilet
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list

# php7.x
apt-get update
apt-get -y install php${PHP_V} php${PHP_V}-common php${PHP_V}-cli php${PHP_V}-fpm php${PHP_V}-mbstring php${PHP_V}-intl php${PHP_V}-xml php${PHP_V}-mysql php${PHP_V}-curl php${PHP_V}-sqlite php-sqlite3 php${PHP_V}-gd php${PHP_V}-imagick php${PHP_V}-opcache php${PHP_V}-zip php${PHP_V}-x php${PHP_V}-tidy php${PHP_V}-xdebug

service php${PHP_V}-fpm restart

# Install Mariadb
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://ftp.hosteurope.de/mirror/mariadb.org/repo/'${MARIADB_V}'/debian stretch main'
sudo apt-get update


DEBIAN_FRONTEND=noninteractive sudo apt-get install -qy mariadb-server

## mysql_secure_installation

mysql -u root <<< "CREATE SCHEMA IF NOT EXISTS ${DB_NAME} DEFAULT CHARACTER SET ${DB_CHARACTER_SET} DEFAULT COLLATE ${DB_COLLATE};"
mysql -u root <<< "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USERNAME}'@localhost IDENTIFIED BY '${DB_PASSWORD}';"

if [ -e /vagrant/${DB_NAME}.sql ]
    then
        echo "importing db, be patient"
        mysql -u ${DB_USERNAME} -p${DB_PASSWORD} ${DB_NAME} < /vagrant/${DB_NAME}.sql
    else
        echo "db backup does not exist! Import manually or run migrations!"
fi

if [ ${HTTP_SERVER} == 'nginx' ]; then
# nginx setup
#
sudo apt-get -qy install nginx

echo "Creating a Self-Signed SSL"
mkdir /etc/${HTTP_SERVER}/certs
cd /etc/${HTTP_SERVER}/certs
sudo openssl genrsa -out "vagrantbox.key" 2048
sudo openssl req -new -key "vagrantbox.key" -out "vagrantbox.csr" -subj "/C=${SSL_C}/ST=${SSL_ST}/L=${SSL_L}/O=${SSL_O}/OU=${SSL_O}/CN=vagrantbox/emailAddress=${EMAIL}"
sudo openssl x509 -req -days 365 -in "vagrantbox.csr" -signkey "vagrantbox.key" -out "vagrantbox.crt"

cat <<EOF > /etc/nginx/sites-available/default
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;

    root /vagrant/webroot;

    index index.php;

    server_name _;

    ssl_certificate /etc/nginx/certs/vagrantbox.crt;
    ssl_certificate_key /etc/nginx/certs/vagrantbox.key;

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    location ~ \.php\$ {
        try_files \$uri =404;
        include fastcgi_params;
        fastcgi_pass unix:/run/php/php${PHP_V}-fpm.sock;
        fastcgi_index index.php;
        fastcgi_intercept_errors on;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}
EOF
echo "run nginx restart"
sudo service nginx restart
else 
sudo apt-get -qy install apache2 apache2-utils libapache2-mod-php${PHP_V}
    if ! [ -L /var/www/html ]; then
        rm -rf /var/www/html
        ln -fs /vagrant /var/www/html
        sudo sed -i 's_DocumentRoot /var/www/html_DocumentRoot /var/www/html/webroot_' /etc/apache2/sites-available/000-default.conf
        sudo sed -i 's_AllowOverride None_AllowOverride All_' /etc/apache2/apache2.conf
    fi
sudo chown www-data:www-data /var/www/html/webroot/ -R
sudo a2enmod rewrite
sudo service php${PHP_V}-fpm restart
sudo systemctl enable apache2
sudo service apache2 restart
fi

# Install Composer && CakePHP
sudo -u vagrant bash << COMPOSER
    cd /vagrant
    EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
      then
        >&2 echo 'ERROR: Invalid installer signature'
        rm composer-setup.php
        exit 1
    fi

    php composer-setup.php --quiet
    php -r "unlink('composer-setup.php');"
    sudo mv composer.phar /usr/local/bin/composer
    cd /usr/local/bin/
    sudo chmod 755 composer
COMPOSER

if [[ ! -e /vagrant/composer.json ]]; then

sudo -u vagrant bash << CAKE
    cd /vagrant
    composer create-project --prefer-dist --no-interaction cakephp/app app
    composer require --dev dereuromark/cakephp-ide-helper
    composer require --dev phpstan/phpstan
CAKE

echo "Moving CakePHP files ..."
cd /vagrant/app && cp -rf * .[^.]* ..
rm -rf /vagrant/app
else
sudo -u vagrant bash << CAKE
    cd /vagrant
    composer install
CAKE
fi

# install php cs fixer
cd /vagrant
sudo wget https://cs.symfony.com/download/php-cs-fixer-v2.phar -O php-cs-fixer
sudo chmod a+x php-cs-fixer
sudo mv php-cs-fixer /usr/local/bin/php-cs-fixer
sudo apt-get autoremove
sudo apt-get clean

#install platform CLI
sudo curl -sS https://platform.sh/cli/installer | php

# ssh banner
sudo bash -c "figlet CakePHP >  /etc/motd"

sudo -u vagrant bash << BASHRC
    echo "cd /vagrant" >> ~/.bashrc
    echo "/vagrant/bin/cake migrations status"  >> ~/.bashrc
BASHRC
