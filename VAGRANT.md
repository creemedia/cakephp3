# vagrant-cakephp
Debian Vagrant Box and LEMP / LAMP provisioning incl. last CakePHP https://cakephp.org/


## README
- clone to your new project dir
- install Vagrant [https://www.vagrantup.com/] and VirtualBox

## Contain
- Vagrant box: debian/contrib-stretch64 
- provisions: LEMP / LAMP + CakePHP + SSL

## Before Vagrant Up
- setup IP, HOSTNAME, ...

## Vagrant Up (provisioning)
- install LEMP / LAMP (Linux, Nginx or Apache, MariaDB, PHP 7.*)
- install XDEBUG
- install SSL
- install PHP CS FIXER (use: `php-cs-fixer fix src/`)
- install CakePHP in project directory (if composer.json does not exists)
- CakePHP is ready for new development

## Vagrant SSH
 - automaticly change directory runing `cd /vagrant`
 - check migrations status `bin/cake migrations status`

## Vagrant destroy
- backup db to root dir